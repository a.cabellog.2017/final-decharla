# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

# Datos

* Nombre: Adrián Cabello Garrido
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: acabello
* Cuenta URJC: a.cabellog.2017
* Video básico (url): https://youtu.be/69QNWrk67XU
* Video parte opcional (url): https://youtu.be/2lse6kycpOE
* Despliegue (url): http://decharla.ddns.net:8000/DeCharla
* Contraseñas: 1234
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
Última Actualización: He conseguido que la aplicación esté funcionando en una raspberry que tenía por casa, estos últimos commits han sido para conseguir realizar el despliege. En concreto es una reaspberry 3b+ connectada via ethernet a un router mediante el uso de Dynamic DNS.

Este informe describe una aplicación de mensajería que ofrece a las sesiones identificadas la capacidad de comunicarse en tiempo real. Para acceder a los recursos de la aplicación, es necesario autenticarse utilizando una contraseña válida.

Una vez que se ha ingresado la contraseña en el campo de "Inicio de sesión", los usuarios son redirigidos a la página de inicio (HOME), que incluirá lo siguiente:

    Diversas opciones de navegación (CHATS, Ayuda, Config y Admin).
    Habrá una barra lateral que permite crear salas exclusivamente en la página principal.
    Un pie de página que mostrará el recuento total de mensajes, salas activas y número total de imágenes.
    Este pie de página estará visible en todas las páginas de la aplicación.
    Se proporcionará una lista de las salas activas con enlaces a su estado estático, dinámico y JSON.

Cuando se acceda a la página de un chat, en la parte inferior se encontrarán dos botones para acceder al chat en formato dinámico y en formato JSON. Al hacer clic en cada uno de estos botones, se redirigirá a la misma página pero con la funcionalidad descrita agregada. También habrá un botón para acceder al tipo de chat correspondiente.

Una vez dentro de una sala de chat, ya sea estática o dinámica, se podrá enviar un mensaje o una imagen. Para enviar una imagen, será necesario incluir la URL en el formulario y hacer clic en el botón correspondiente que verificará si el mensaje es una imagen o simplemente texto.

Además, en las salas estáticas de chat, se podrá realizar una solicitud "PUT" que incluirá mensajes en formato "XML". Esta solicitud se realizará a través de la aplicación REST, donde se especificará que se desea hacer una petición PUT a la dirección URL de la sala en la que se desea enviar el mensaje. El mensaje deberá tener un formato XML para poder analizarlo correctamente y obtener la información necesaria para agregar el mensaje a la sala.

Los campos requeridos en el "XML" son los siguientes:

    Encabezado del formato XML.
    Campo messages: Contendrá los mensajes que se deseen incluir.
    Campo message: Contendrá el mensaje a incluir y deberá incluir la etiqueta "isimg", que puede ser "True" si el mensaje es una imagen o "False" si el mensaje es texto.
    Campo text: Contendrá el texto del mensaje o la URL de la imagen.

Al hacer clic en el botón Ayuda, se mostrará una página con secciones desplegables que explicarán el funcionamiento de cada recurso de la aplicación.

Al hacer clic en el botón Config, se mostrará una página donde se podrá personalizar el nombre de la sesión, el tipo de fuente y el tamaño de la fuente de la aplicación.

Por último, al hacer clic en el botón Admin, se abrirá la interfaz del "Admin Site" característico de cualquier aplicación DJANGO.

Además de los botones de navegación, en la barra lateral derecha se puede hacer clic en el botón "Crear Sala", que redirigirá a una página donde se podrá crear una sala con un nombre elegido por el usuario.

## Lista partes opcionales

* Favicon ico
* Permitir cerrar la sesión
* Permitir votas las salas
