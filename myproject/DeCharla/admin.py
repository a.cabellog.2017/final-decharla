from django.contrib import admin
from .models import Sala, Mensaje, Contrasena

# Register your models here.
admin.site.register(Sala)
admin.site.register(Mensaje)
admin.site.register(Contrasena)
