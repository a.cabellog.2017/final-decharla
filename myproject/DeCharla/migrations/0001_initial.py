# Generated by Django 3.1.7 on 2023-07-05 21:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contrasena',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pswd_valida', models.CharField(max_length=16)),
            ],
        ),
        migrations.CreateModel(
            name='Sala',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('fecha_creacion', models.DateTimeField()),
                ('votos', models.IntegerField(default=0)),
                ('voto_dado', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('fuente', models.CharField(default='Arial', max_length=16)),
                ('tam_fuente', models.CharField(default='Mediana', max_length=16)),
                ('nombre', models.CharField(max_length=60)),
                ('id', models.IntegerField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Mensaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_img', models.BooleanField(default=False)),
                ('contenido', models.TextField()),
                ('fecha_creacion', models.DateTimeField()),
                ('autor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.usuario')),
                ('sala', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.sala')),
            ],
        ),
    ]
