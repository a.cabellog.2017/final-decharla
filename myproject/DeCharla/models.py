from django.db import models

# Create your models here.
class Usuario(models.Model):
    FUENTE = [
        "Arial",
        "Times New Roman",
        "Helvetica",
        "Verdana"
    ]
    TAM_FUENTE = [
        "Mediana",
        "Pequeña",
        "Grande"
    ]
    fuente = models.CharField(default="Arial", max_length=16)
    tam_fuente = models.CharField(default="Mediana", max_length=16)
    nombre = models.CharField(max_length=60)
    id = models.IntegerField(primary_key=True)
    def __str__(self):
        return self.nombre

class Contrasena(models.Model):
    pswd_valida = models.CharField(max_length=16)
    def __str__(self):
        return "contraseña: " + str(self.pswd_valida)

class Sala (models.Model):
    nombre = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField()
    votos = models.IntegerField(default=0)
    voto_dado = models.BooleanField(default=False)
    def __str__(self):
        return "El nombre de la sala es: " + str(self.nombre)
class Mensaje (models.Model):
    autor = models.ForeignKey('Usuario', on_delete=models.CASCADE)
    sala = models.ForeignKey('Sala', on_delete=models.CASCADE)
    tipo_img = models.BooleanField(default=False)
    contenido = models.TextField()
    fecha_creacion = models.DateTimeField()
    def __str__(self):
        return "alguien comentó: " + str(self.contenido)
