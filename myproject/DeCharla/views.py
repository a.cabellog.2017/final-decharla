import datetime
import random

from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import loader
from .models import Sala, Mensaje, Contrasena, Usuario
from django.http import JsonResponse


class Estadisticas:
    def __init__(self):
        self.text = Mensaje.objects.filter(tipo_img=False).count()
        self.img = Mensaje.objects.filter(tipo_img=True).count()
        self.usuarios = Usuario.objects.all().count()


# Create your views here.
def verificar_sesion(view_func):
    def wrapper(request, *args, **kwargs):
        if 'id_usuario' not in request.COOKIES:
            return redirect('/DeCharla/login')
        return view_func(request, *args, **kwargs)
    return wrapper


def render_with_user(request, template_name, context=None):
    if context is None:
        context = {}
    id_usuario = request.COOKIES.get('id_usuario')
    if id_usuario and Usuario.objects.filter(id=id_usuario).exists():
        usuario = Usuario.objects.get(id=id_usuario)
        context['usuario'] = usuario
    template = loader.get_template(template_name)
    return HttpResponse(template.render(context, request))


def login(request):
    id_usuario = request.COOKIES.get('id_usuario')
    if id_usuario and Usuario.objects.filter(id=id_usuario):
        return redirect('/DeCharla')
    cntr_incorrecta = False
    if request.method == 'POST':
        input = request.POST['password']

        try:
            Contrasena.objects.get(pswd_valida=input)
            id = random.randint(1, 10000)
            nombre = "Anónimo" + str(id)
            nuevo_usuario = Usuario(nombre=nombre, id=id)
            nuevo_usuario.save()
            respuesta = redirect('/DeCharla/')
            respuesta.set_cookie("id_usuario", id)
            return respuesta
        except Contrasena.DoesNotExist:
            cntr_incorrecta = True
    context = {
        'cntr_incorrecta': cntr_incorrecta,
        'id_usuario': id_usuario,
        'estadisticas': Estadisticas()
    }
    return render_with_user(request, 'DeCharla/login.html', context)


@verificar_sesion
def logout(request):
    if 'id_usuario' in request.COOKIES:
        response = redirect('/DeCharla/login')
        response.delete_cookie('id_usuario')
        return response
    else:
        return redirect('/DeCharla/login')


@verificar_sesion
def config(request):
    id_usuario = request.COOKIES['id_usuario']
    usuario = Usuario.objects.get(id=id_usuario)
    if request.method == 'POST':
        nuevo_nombre = request.POST.get('nuevo_nombre')
        fuente = request.POST.get('fuente')
        tam_fuente = request.POST.get('tam_fuente')
        if tam_fuente == "Pequeña":
            tam_fuente = "x-small"
        elif tam_fuente == "Grande":
            tam_fuente = "x-large"
        else:
            tam_fuente = "medium"
        if nuevo_nombre == '':
            nuevo_nombre = usuario.nombre
        usuario.nombre = nuevo_nombre
        usuario.fuente = fuente
        usuario.tam_fuente = tam_fuente
        usuario.save()
    context = {
        'usuario': usuario,
        'estadisticas': Estadisticas()
    }
    return render_with_user(request, 'DeCharla/config.html', context)


@verificar_sesion
def ayuda(request):
    context = {
        'estadisticas': Estadisticas()
    }
    return render_with_user(request, 'DeCharla/ayuda.html', context)


@verificar_sesion
def crear_mensaje(request, sala, usuario):

    fecha = datetime.datetime.now()
    tipo_img = request.POST.get('tipo_img', False)
    if tipo_img == 'on':
        tipo_img = True
    else:
        tipo_img = False
    contenido = str(request.POST.get('contenido', ''))
    nuevo_mensaje = Mensaje(sala=sala, fecha_creacion=fecha, tipo_img=tipo_img, contenido=contenido, autor=usuario)
    nuevo_mensaje.save()
    return redirect('/DeCharla/' + sala.nombre)


@verificar_sesion
def crear_mensaje_dinc(request, sala, usuario):

    fecha = datetime.datetime.now()
    tipo_img = request.POST.get('tipo_img', False)
    if tipo_img == 'on':
        tipo_img = True
    else:
        tipo_img = False
    contenido = str(request.POST.get('contenido', ''))
    nuevo_mensaje = Mensaje(sala=sala, fecha_creacion=fecha, tipo_img=tipo_img, contenido=contenido, autor=usuario)
    nuevo_mensaje.save()
    return redirect('/DeCharla/' + sala.nombre + '/dinc')


@verificar_sesion
def sala(request, nombre_sala):
    info_sala = Sala.objects.get(nombre=nombre_sala)
    lista_mensajes = reversed(Mensaje.objects.filter(sala=info_sala))
    id_usuario = request.COOKIES['id_usuario']
    usuario = Usuario.objects.get(id=id_usuario)
    if request.method == 'POST':
        if request.POST['action'] == 'dar_voto':
            if not info_sala.voto_dado:
                info_sala.votos += 1
                info_sala.voto_dado = True
                info_sala.save()
        else:
            resp = crear_mensaje(request, info_sala, usuario)
            return resp
    context = {
        'lista_mensajes': lista_mensajes,
        'info_sala': info_sala,
        'estadisticas': Estadisticas(),
    }
    return render_with_user(request, 'DeCharla/sala.html', context)


@verificar_sesion
def saladinamic(request, nombre_sala):
    info_sala = Sala.objects.get(nombre=nombre_sala)
    lista_mensajes = reversed(Mensaje.objects.filter(sala=info_sala))
    id_usuario = request.COOKIES['id_usuario']
    usuario = Usuario.objects.get(id=id_usuario)
    if request.method == 'POST':
        if request.POST['action'] == 'dar_voto':
            if not info_sala.voto_dado:
                info_sala.votos += 1
                info_sala.voto_dado = True
                info_sala.save()
            else:
                resp = crear_mensaje_dinc(request, info_sala, usuario)
                return resp
    context = {
        'lista_mensajes': lista_mensajes,
        'info_sala': info_sala,
        'estadisticas': Estadisticas(),
    }
    return render_with_user(request, 'DeCharla/saladinamic.html', context)


@verificar_sesion
def index(request):
    lista_salas = Sala.objects.all()[:5]
    context = {
        'lista_salas': lista_salas,
        'estadisticas': Estadisticas()
    }
    if request.method == 'POST':
        nombresala = request.POST.get("nombre_sala", "")
        if nombresala:
            fecha = datetime.datetime.now()
            try:
                sala = Sala.objects.get(nombre=nombresala)
            except Sala.DoesNotExist:
                sala = Sala(nombre=nombresala, fecha_creacion=fecha)
                sala.save()
            return redirect('/DeCharla/' + sala.nombre)
    else:
        return render_with_user(request, 'DeCharla/index.html', context)


@verificar_sesion
def exportar_json(request, nombre_sala):
    info_sala = Sala.objects.get(nombre=nombre_sala)
    lista_mensajes = Mensaje.objects.filter(sala=info_sala).values('autor__nombre', 'contenido', 'tipo_img', 'fecha_creacion')
    json_data = {
        'sala': nombre_sala,
        'mensajes': list(lista_mensajes),
    }
    return JsonResponse(json_data)
