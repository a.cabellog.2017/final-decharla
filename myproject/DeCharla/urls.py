from django.urls import path
from . import views

urlpatterns = [
        path('', views.index),
        path('login', views.login),
        path('logout', views.logout),
        path('config', views.config),
        path('ayuda', views.ayuda),
        path('<str:nombre_sala>', views.sala),
        path('<str:nombre_sala>/dinc', views.saladinamic),
        path('<str:nombre_sala>/json', views.exportar_json),
]
